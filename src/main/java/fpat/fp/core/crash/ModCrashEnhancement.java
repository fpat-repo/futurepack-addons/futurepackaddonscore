package fpat.fp.core.crash;

import fpat.fp.core.config.LoggingConfigurations;

public class ModCrashEnhancement extends BaseCrashEnhancement
{
	private static final String MOD_VERSION = LoggingConfigurations.CHANNEL + ' ' + LoggingConfigurations.VERSION + " for Forge " + // WHAT?
			net.minecraftforge.common.ForgeVersion.majorVersion + '.' // majorVersion
			+ net.minecraftforge.common.ForgeVersion.minorVersion + '.' // minorVersion
			+ net.minecraftforge.common.ForgeVersion.revisionVersion + '.' // revisionVersion
			+ net.minecraftforge.common.ForgeVersion.buildVersion;

	public ModCrashEnhancement( final CrashInfo output )
	{
		super( "Futurepack Applied Energistics 2 Version", MOD_VERSION );
	}
}