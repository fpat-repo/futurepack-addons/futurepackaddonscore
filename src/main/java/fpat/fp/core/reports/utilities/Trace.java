package fpat.fp.core.reports.utilities;

public enum Trace {
	
	
	CORE( "Core", null )
	{
		@Override 
		public boolean isVisible()
		{
			return false;
		}
	},
	DEBUG_LOGGING( "DebugLogging", Constants.CATEGORY_TRACE, false ),
	LOGGING( "Logging", Constants.CATEGORY_TRACE, true );

	private final String key;
	private final String category;
	private final boolean enabled;
	
	Trace( final String key, final String cat )
	{
		this( key, cat, true );
	}

	Trace( final String key, final String cat, final boolean enabled )
	{
		this.key = key;
		this.category = cat;
		this.enabled = enabled;
	}
	
	public boolean isVisible()
	{
		return true;
	}
	
	public String key()
	{
		return this.key;
	}

	public String category()
	{
		return this.category;
	}

	public boolean isEnabled()
	{
		return this.enabled;
	}

	private enum Constants
	{
		
		;
		
		private static final String CATEGORY_TRACE = "Tracing";
	}
}
