package fpat.fp.core.reports;

import javax.annotation.Nonnull;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessage;

import fpat.fp.core.reports.utilities.Platform;
import fpat.fp.core.reports.utilities.Trace;
import fpat.fp.core.config.LoggingConfigurations;
import net.minecraft.util.math.BlockPos;



public final class BaseLogger
{
	private static final String LOGGER_PREFIX = "FPAT-Core:";
	private static final String SERVER_SUFFIX = "Server";
	private static final String CLIENT_SUFFIX = "Client";

	private static final Logger SERVER = LogManager.getFormatterLogger( LOGGER_PREFIX + SERVER_SUFFIX );
	private static final Logger CLIENT = LogManager.getFormatterLogger( LOGGER_PREFIX + CLIENT_SUFFIX );

	private static final String BLOCK_UPDATE = "Block Update of %s @ ( %s )";

	private static final String DEFAULT_EXCEPTION_MESSAGE = "Exception: ";

	private BaseLogger()
	{
	}

	/**
	 * Returns a {@link Logger} logger suitable for the effective side (client/server).
	 *
	 * @return a suitable logger instance
	 */
	private static Logger getLogger()
	{
		return Platform.isServer() ? SERVER : CLIENT;
	}

	/**
	 * Indicates of the global log is enabled or disabled.
	 *
	 * By default it is enabled.
	 *
	 * @return true when the log is enabled.
	 */
	public static boolean isLogEnabled()
	{
		return LoggingConfigurations.instance() == null || LoggingConfigurations.instance().isFeatureEnabled( Trace.LOGGING );
	}

	/**
	 * Logs a formatted message with a specific log level.
	 *
	 * This uses {@link String#format(String, Object...)} as opposed to the {@link ParameterizedMessage} to allow a more
	 * flexible formatting.
	 *
	 * The output can be globally disabled via the configuration file.
	 *
	 * @param level the intended level.
	 * @param message the message to be formatted.
	 * @param params the parameters used for {@link String#format(String, Object...)}.
	 */
	public static void log( @Nonnull final Level level, @Nonnull final String message, final Object... params )
	{
		if( BaseLogger.isLogEnabled() )
		{
			final String formattedMessage = String.format( message, params );
			final Logger logger = getLogger();

			logger.log( level, formattedMessage );
		}
	}

	/**
	 * Log an exception with a custom message formated via {@link String#format(String, Object...)}
	 *
	 * Similar to {@link BaseLogger#log(Level, String, Object...)}.
	 *
	 * @see BaseLogger#log(Level, String, Object...)
	 *
	 * @param level the intended level.
	 * @param exception
	 * @param message the message to be formatted.
	 * @param params the parameters used for {@link String#format(String, Object...)}.
	 */
	public static void log( @Nonnull final Level level, @Nonnull final Throwable exception, @Nonnull String message, final Object... params )
	{
		if( BaseLogger.isLogEnabled() )
		{
			final String formattedMessage = String.format( message, params );
			final Logger logger = getLogger();

			logger.log( level, formattedMessage, exception );
		}
	}

	/**
	 * @see BaseLogger#log(Level, String, Object...)
	 * @param format
	 * @param params
	 */
	public static void info( @Nonnull final String format, final Object... params )
	{
		log( Level.INFO, format, params );
	}

	/**
	 * Log exception as {@link Level#INFO}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 */
	public static void info( @Nonnull final Throwable exception )
	{
		log( Level.INFO, exception, DEFAULT_EXCEPTION_MESSAGE );
	}

	/**
	 * Log exception as {@link Level#INFO}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 * @param message
	 */
	public static void info( @Nonnull final Throwable exception, @Nonnull final String message )
	{
		log( Level.INFO, exception, message );
	}

	/**
	 * @see BaseLogger#log(Level, String, Object...)
	 * @param format
	 * @param params
	 */
	public static void warn( @Nonnull final String format, final Object... params )
	{
		log( Level.WARN, format, params );
	}

	/**
	 * Log exception as {@link Level#WARN}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 */
	public static void warn( @Nonnull final Throwable exception )
	{
		log( Level.WARN, exception, DEFAULT_EXCEPTION_MESSAGE );
	}

	/**
	 * Log exception as {@link Level#WARN}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 * @param message
	 */
	public static void warn( @Nonnull final Throwable exception, @Nonnull final String message )
	{
		log( Level.WARN, exception, message );
	}

	/**
	 * @see BaseLogger#log(Level, String, Object...)
	 * @param format
	 * @param params
	 */
	public static void error( @Nonnull final String format, final Object... params )
	{
		log( Level.ERROR, format, params );
	}

	/**
	 * Log exception as {@link Level#ERROR}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 */
	public static void error( @Nonnull final Throwable exception )
	{
		log( Level.ERROR, exception, DEFAULT_EXCEPTION_MESSAGE );
	}

	/**
	 * Log exception as {@link Level#ERROR}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 * @param message
	 */
	public static void error( @Nonnull final Throwable exception, @Nonnull final String message )
	{
		log( Level.ERROR, exception, message );
	}

	/**
	 * Log message as {@link Level#DEBUG}
	 *
	 * @see BaseLogger#log(Level, String, Object...)
	 * @param format
	 * @param data
	 */
	public static void debug( @Nonnull final String format, final Object... data )
	{
		if( BaseLogger.isDebugLogEnabled() )
		{
			log( Level.DEBUG, format, data );
		}
	}

	/**
	 * Log exception as {@link Level#DEBUG}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 */
	public static void debug( @Nonnull final Throwable exception )
	{
		if( BaseLogger.isDebugLogEnabled() )
		{
			log( Level.DEBUG, exception, DEFAULT_EXCEPTION_MESSAGE );
		}
	}

	/**
	 * Log exception as {@link Level#DEBUG}
	 *
	 * @see BaseLogger#log(Level, Throwable, String, Object...)
	 *
	 * @param exception
	 * @param message
	 */
	public static void debug( @Nonnull final Throwable exception, @Nonnull final String message )
	{
		if( BaseLogger.isDebugLogEnabled() )
		{
			log( Level.DEBUG, exception, message );
		}
	}

	/**
	 * Use to check for an enabled debug log.
	 *
	 * Can be used to prevent the execution of debug logic.
	 *
	 * @return true when the debug log is enabled.
	 */
	public static boolean isDebugLogEnabled()
	{
		return LoggingConfigurations.instance().isFeatureEnabled( Trace.DEBUG_LOGGING );
	}
}