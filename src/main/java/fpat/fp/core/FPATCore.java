package fpat.fp.core;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import com.google.common.base.Stopwatch;

import fpat.fp.core.config.LoggingConfigurations;
import fpat.fp.core.crash.CrashInfo;
import fpat.fp.core.crash.ModCrashEnhancement;
import fpat.fp.core.reports.BaseLogger;
import futurepack.client.creative.TabFB_Base;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;


@Mod(modid = FPATCore.modID, name = FPATCore.modName, version = FPATCore.modVersion, dependencies = "required-after:fp")
public class FPATCore
{
	public static final String modID = "fpatcore";
	public static final String modName = "Futurepack Addon - Core";
	public static final String modVersion = "Version.version";
	
	@Nonnull
	private static final FPATCore INSTANCE = new FPATCore();

	private File configDirectory;
	private File recipeDirectory;
	
	public FPATCore()
	{
		FMLCommonHandler.instance().registerCrashCallable( new ModCrashEnhancement( CrashInfo.MOD_VERSION ) );
	}
	
	@Nonnull
	@Mod.InstanceFactory
	public static FPATCore instance()
	{
		return INSTANCE;
	}

//	@SidedProxy(modId=FPATCore.modID, clientSide="fpat.fp.core.common.FPATCoreProxyClient", serverSide="fpat.fp.core.common.FPATCoreProxyServer")
//	public static FPATCoreProxyBase proxy;

	@EventHandler
    public void preInit(FMLPreInitializationEvent event) 
    {
	final Stopwatch watch = Stopwatch.createStarted();

	BaseLogger.info( "Pre Initialization ( started )" );
	
	this.configDirectory = new File( event.getModConfigurationDirectory().getPath(), "fpat/core." );

	final File configFile = new File( this.configDirectory, "LoggingConfiguration.cfg" );
	
	LoggingConfigurations.init( configFile );

	BaseLogger.info( "Pre Initialization ( ended after " + watch.elapsed( TimeUnit.MILLISECONDS ) + "ms )" );
    }

	private void startService( final String serviceName, final Thread thread )
	{
		thread.setName( serviceName );
		thread.setPriority( Thread.MIN_PRIORITY );

		BaseLogger.info( "Starting " + serviceName );
		thread.start();
	}
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		final Stopwatch start = Stopwatch.createStarted();
		BaseLogger.info( "Initialization ( started )" );

		BaseLogger.info( "Initialization ( ended after " + start.elapsed( TimeUnit.MILLISECONDS ) + "ms )" );
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		final Stopwatch start = Stopwatch.createStarted();
		BaseLogger.info( "Post Initialization ( started )" );
		

		BaseLogger.info( "Post Initialization ( ended after " + start.elapsed( TimeUnit.MILLISECONDS ) + "ms )" );
	}
	
	@EventHandler
	public void preServerStart(FMLServerAboutToStartEvent event) 
	{
		
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) 
	{		

	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event) 
	{

	}
	
	
	@EventHandler
	public void serverStopped(FMLServerStoppingEvent event) 
	{

	}
}