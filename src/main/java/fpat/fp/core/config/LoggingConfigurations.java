package fpat.fp.core.config;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import com.google.common.collect.Sets;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import fpat.fp.core.FPATCore;
import fpat.fp.core.reports.utilities.*;
import fpat.fp.core.reports.BaseLogger;
import fpat.fp.core.config.utilities.*;

public final class LoggingConfigurations extends Configuration implements IConfigurableObject, IConfigManagerHost
{

	public static final String VERSION = "@version@";
	public static final String CHANNEL = "@aechannel@";
	public static final String PACKET_CHANNEL = "FPAT-Core";

	// Config instance
	private static LoggingConfigurations instance;

	private final IConfigManager settings = new ConfigManager( this );

	private final EnumSet<Trace> featureFlags = EnumSet.noneOf( Trace.class );
	private final File configFile;
	private boolean updatable = false;

	private LoggingConfigurations( final File configFile )
	{
		super( configFile );
		this.configFile = configFile;

		MinecraftForge.EVENT_BUS.register( this );

		this.clientSync();

		for( final Trace feature : Trace.values() )
		{
			if( feature.isVisible() )
			{
				final Property option = this.get( "Features." + feature.category(), feature.key(), feature.isEnabled());

				if( option.getBoolean( feature.isEnabled() ) )
				{
					this.featureFlags.add( feature );
				}
			}
			else
			{
				this.featureFlags.add( feature );
			}
		}

		this.updatable = true;
	}

	public static void init( final File configFile )
	{
		instance = new LoggingConfigurations( configFile );
	}

	public static LoggingConfigurations instance()
	{
		return instance;
	}
	
	private void clientSync()
	{
		
	}


	private String getListComment( final Enum value )
	{
		String comment = null;

		if( value != null )
		{
			final EnumSet set = EnumSet.allOf( value.getClass() );

			for( final Object Oeg : set )
			{
				final Enum eg = (Enum) Oeg;
				if( comment == null )
				{
					comment = "Possible Values: " + eg.name();
				}
				else
				{
					comment += ", " + eg.name();
				}
			}
		}

		return comment;
	}

	public boolean isFeatureEnabled( final Trace f )
	{
		return this.featureFlags.contains( f );
	}

	public boolean areFeaturesEnabled( Collection<Trace> features )
	{
		return this.featureFlags.containsAll( features );
	}


	@Override
	public Property get( final String category, final String key, final String defaultValue, final String comment, final Property.Type type )
	{
		final Property prop = super.get( category, key, defaultValue, comment, type );

		if( prop != null )
		{
			if( !category.equals( "Client" ) )
			{
				prop.setRequiresMcRestart( true );
			}
		}

		return prop;
	}

	@Override
	public void save()
	{
		if( this.hasChanged() )
		{
			super.save();
		}
	}

	@SubscribeEvent
	public void onConfigChanged( final ConfigChangedEvent.OnConfigChangedEvent eventArgs )
	{
		if( eventArgs.getModID().equals( FPATCore.modID ) )
		{
			this.clientSync();
		}
	}

	public String getFilePath()
	{
		return this.configFile.toString();
	}

	@Override
	public IConfigManager getConfigManager()
	{
		return this.settings;
	}

	@Override
	public void updateSetting(IConfigManager manager, Enum settingName, Enum newValue)
	{


		if( this.updatable )
		{
			this.save();
		}
	}
}