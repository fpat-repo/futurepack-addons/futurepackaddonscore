package fpat.fp.core.config.utilities;

import java.util.EnumSet;

import javax.annotation.Nonnull;


public enum Settings
{
	;
	
	;

	private final EnumSet<? extends Enum<?>> values;
	

	Settings( @Nonnull final EnumSet<? extends Enum<?>> possibleOptions )
	{
		if( possibleOptions.isEmpty() )
		{
			throw new IllegalArgumentException( "Tried to instantiate an empty setting." );
		}

		this.values = possibleOptions;
	}

	public EnumSet<? extends Enum<?>> getPossibleValues()
	{
		return this.values;
	}

}