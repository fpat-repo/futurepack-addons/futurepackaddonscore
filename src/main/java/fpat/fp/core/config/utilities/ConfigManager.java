package fpat.fp.core.config.utilities;

import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

import fpat.fp.core.reports.BaseLogger;

import net.minecraft.nbt.NBTTagCompound;

public final class ConfigManager implements IConfigManager
{
	private final Map<Settings, Enum<?>> settings = new EnumMap<>( Settings.class );
	private final IConfigManagerHost target;

	public ConfigManager( final IConfigManagerHost tile )
	{
		this.target = tile;
	}

	@Override
	public Set<Settings> getSettings()
	{
		return this.settings.keySet();
	}

	@Override
	public void registerSetting( final Settings settingName, final Enum defaultValue )
	{
		this.settings.put( settingName, defaultValue );
	}

	@Override
	public Enum<?> getSetting( final Settings settingName )
	{
		final Enum<?> oldValue = this.settings.get( settingName );

		if( oldValue != null )
		{
			return oldValue;
		}

		throw new IllegalStateException( "Invalid Config setting. Expected a non-null value for " + settingName );
	}

	@Override
	public Enum<?> putSetting( final Settings settingName, final Enum newValue )
	{
		final Enum<?> oldValue = this.getSetting( settingName );
		this.settings.put( settingName, newValue );
		this.target.updateSetting( this, settingName, newValue );
		return oldValue;
	}

	/**
	 * save all settings using config manager.
	 *
	 * @param tagCompound to be written to compound
	 */
	@Override
	public void writeToNBT( final NBTTagCompound tagCompound )
	{
		for( final Map.Entry<Settings, Enum<?>> entry : this.settings.entrySet() )
		{
			tagCompound.setString( entry.getKey().name(), this.settings.get( entry.getKey() ).toString() );
		}
	}

	/**
	 * read all settings using config manager.
	 *
	 * @param tagCompound to be read from compound
	 */
	@Override
	public void readFromNBT( final NBTTagCompound tagCompound )
	{
		for( final Map.Entry<Settings, Enum<?>> entry : this.settings.entrySet() )
		{
			try
			{
				if( tagCompound.hasKey( entry.getKey().name() ) )
				{
					String value = tagCompound.getString( entry.getKey().name() );

					final Enum<?> oldValue = this.settings.get( entry.getKey() );

					final Enum<?> newValue = Enum.valueOf( oldValue.getClass(), value );

					this.putSetting( entry.getKey(), newValue );
				}
			}
			catch( final IllegalArgumentException e )
			{
				BaseLogger.debug( e );
			}
		}
	}
}