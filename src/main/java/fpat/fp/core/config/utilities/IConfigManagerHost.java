package fpat.fp.core.config.utilities;


public interface IConfigManagerHost
{

	void updateSetting( IConfigManager manager, Enum settingName, Enum newValue );
}