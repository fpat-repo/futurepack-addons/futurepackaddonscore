package fpat.fp.core.config.utilities;

public interface IConfigurableObject
{
	IConfigManager getConfigManager();
}