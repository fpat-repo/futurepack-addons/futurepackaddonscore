package fpat.fp.core.config.utilities;

import java.util.Set;

import net.minecraft.nbt.NBTTagCompound;


/**
 * Used to adjust settings on an object,
 *
 * Obtained via {@link IConfigurableObject}
 */
public interface IConfigManager
{

	/**
	 * get a list of different settings
	 *
	 * @return enum set of settings
	 */
	Set<Settings> getSettings();

	/**
	 * used to initialize the configuration manager, should be called for all settings.
	 *
	 * @param settingName name of setting
	 * @param defaultValue default value of setting
	 */
	void registerSetting( Settings settingName, Enum<?> defaultValue );

	/**
	 * Get Value of a particular setting
	 *
	 * @param settingName name of setting
	 *
	 * @return value of setting
	 */
	Enum<?> getSetting( Settings settingName );

	/**
	 * Change setting
	 *
	 * @param settingName to be changed setting
	 * @param newValue new value for setting
	 *
	 * @return changed setting
	 */
	Enum<?> putSetting( Settings settingName, Enum<?> newValue );

	/**
	 * write all settings to the NBT Tag so they can be read later.
	 *
	 * @param destination to be written nbt tag
	 */
	void writeToNBT( NBTTagCompound destination );

	/**
	 * Only works after settings have been registered
	 *
	 * @param src to be read nbt tag
	 */
	void readFromNBT( NBTTagCompound src );
}